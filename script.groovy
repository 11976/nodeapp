def incrementVersion() {
    echo "Incrementing version.."
    
    sh '''
        # Read the current version from package.json
        CURRENT_VERSION=$(node -p "require('./app/package.json').version")

        # Increment the version
        MAJOR_VERSION=$(echo $CURRENT_VERSION | cut -d. -f1)
        MINOR_VERSION=$(echo $CURRENT_VERSION | cut -d. -f2)
        PATCH_VERSION=$(echo $CURRENT_VERSION | cut -d. -f3)
        NEXT_VERSION=$((PATCH_VERSION + 1))
        NEW_VERSION="$MAJOR_VERSION.$MINOR_VERSION.$NEXT_VERSION"

        # Update the version in package.json
        sed -i "s/\"version\": \"$CURRENT_VERSION\"/\"version\": \"$NEW_VERSION\"/" ./app/package.json
    '''
    
    def matcher = readFile('./app/package.json') =~ '"version": "(.+)"'
    def version = matcher[0][1]
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}

def BuildApp() {
    echo "building the app"
    sh "cd app && npm install"
    sh "cd app && npm ci"
}

def buildPushImage() {
    echo "building the docker  image..."
    withCredentials([usernamePassword(credentialsId: 'Docker-Hub-Credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t youssefzhioua/my-repo:${IMAGE_NAME} ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push youssefzhioua/my-repo:${IMAGE_NAME}"
    }
} 



return this